#!/bin/bash
set -e
set -x
HOST=$1

hostname $HOST
echo $HOST > /etc/hostname
