#!/bin/bash
set -e
set -x
USER=$1
PASS=$2

echo $USER:$PASS | chpasswd
chage -d 0 $USER
