#!/bin/bash
set -e
set -x

apt-get -qq update
apt-get -qq purge nano
apt-get -qq install traceroute curl tcpdump tshark build-essential git xinetd telnetd

sed -i '/Update-Package-Lists/ s/"1"/"0"/' /etc/apt/apt.conf.d/10periodic

cat <<EOT > /etc/inetd.conf
telnet stream tcp nowait telnetd /usr/sbin/tcpd /usr/sbin/in.telnetd
EOT

cat <<EOT > /etc/xinetd.conf
defaults
{
  instances = 60
  log_type = SYSLOG authpriv
  log_on_success = HOST PID
  log_on_failure = HOST
  cps = 25 30
}
includedir /etc/xinetd.d
EOT

service xinetd restart
