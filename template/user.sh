#!/bin/bash
set -e
set -x
USER=$1

adduser --disabled-password --gecos "" $USER
echo %$USER ALL=NOPASSWD:ALL > /etc/sudoers.d/$USER
chmod 0440 /etc/sudoers.d/$USER
usermod -a -G sudo $USER
