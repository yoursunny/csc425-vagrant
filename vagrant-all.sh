#!/bin/bash
# Execute vagrant command on all VM groups.
# Examples:
#  ./vagrant-all.sh up
#  ./vagrant-all.sh halt
#  ./vagrant-all.sh destroy -f

for D in G*; do
  pushd $D >/dev/null
  vagrant "$@"
  popd >/dev/null
done
