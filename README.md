# CSC425 project virtual machines

This repository contains scripts to setup CSC425 project virtual machines.

Copyright 2016 Arizona Board of Regents

## Configuration

1. Install VirtualBox and Vagrant on a host machine.
2. Copy `config.py.sample` as `config.py`, edit according to comments.
3. Copy `template/ssh_config.sample.in` as `template/ssh_config.in`, edit as necessary.
4. Copy `template/email.sample.in` as `template/email.in`, edit as necessary.

## Allocation

1. Create `alloc.tsv` file.
2. Each line is a TAB-separated record with 2 columns.
   The first column is the VM group id, which should be in `GROUP_RANGE` as defined in `config.py`.
   The second column is a username.
3. Execute `./add-all.sh` to create Vagrantfile for all allocations.

Note:

* VM group id must be unique.
* After `./add-all.sh` has been executed, you may add new allocations by add records into `alloc.tsv`,
  but existing allocations should not be modified or deleted (they have no effect).
* In case you need to reclaim resources, cd into the VM group directory (`Gxx`),
  execute `vagrant destroy -f`, and delete the VM group directory.
  After that, you may modify or delete the allocation record.
  Warning: do not delete VM group directory without destroying the VMs.

## Operations on all VMs

Start:

    ./vagrant-all.sh up

Shutdown all VMs:

    ./vagrant-all.sh halt

Destroy all VMs: (careful!)

    ./vagrant-all.sh destroy -f

It's recommended to run these commands with `screen` or `nohup`.

## Operations on single VM group

A *VM group* is a set of VMs allocated to a single user.
Its configuration is stored in a directory named `Gxx` where xx is the VM group id.
cd into the VM group directory, and you may run any `vagrant` command to operate on the group.

Reboot:

    vagrant reload

Reset to initial password:

    vagrant provision --provision-with password
