#!/bin/bash

while read -r -a L; do
  GROUP=${L[0]}
  USER=${L[1]}
  PASSWORD=$(makepasswd)

  if ! [[ -d G$GROUP ]]; then
    ./add.py --group="$GROUP" --user="$USER" --password="$PASSWORD"
  fi
done < alloc.tsv
