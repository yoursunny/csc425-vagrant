#!/usr/bin/python2

import sys
import os

import config

def parseCommandLine():
    import argparse
    parser = argparse.ArgumentParser(description="Add a VM group.")
    parser.add_argument("--group", type=int, required=True,
                        choices=config.GROUP_RANGE,
                        help="group id")
    parser.add_argument("--user", required=True,
                        help="username")
    parser.add_argument("--password", required=True,
                        help="password")
    args = parser.parse_args()
    return args

def run(args):
    dirname = "G%d" % args.group
    try:
        os.mkdir(dirname)
    except OSError:
        sys.stderr.write("VM group already exists\n")
        exit(1)

    d = dict(
      nameA="vm%d" % (args.group + 0),
      portA=config.PORT_BASE + args.group + 0,
      nameB="vm%d" % (args.group + 1),
      portB=config.PORT_BASE + args.group + 1,
      net="net%d" % args.group,
      user=args.user,
      password=args.password,
      hostMachine=config.HOST_MACHINE
    )

    ssh_config = None
    for filename in ["Vagrantfile", "ssh_config", "email"]:
        with open("template/%s.in" % filename, "r") as f:
            template = f.read()
        sub = d
        if filename == "email":
            sub.update(config.EMAIL_EXTRA)
            sub["ssh_config"] = ssh_config
        body = template % sub
        if filename == "ssh_config":
            ssh_config = body
        with open("%s/%s" % (dirname, filename), "w") as f:
            f.write(body)

if __name__ == "__main__":
    args = parseCommandLine()
    run(args)
